#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

using namespace std;

class task3 {
private:
	string mode, text, alph, language;
	const string alph_eng_high = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
	const string alph_eng_low = "abcdefghiklmnopqrstuvwxyz";
	const string alph_rus = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ_,.";
    ifstream file;
    int a, r;
    bool win_cr, left_side_rot;
public:
	task3(char *f_name) {
		ifstream file(f_name);
	    if (!file.is_open()) {
	        cerr << "file cant be open!" << endl;
	    	exit(0);
	    }
	    getline(file, mode);
	    if (mode.length() == 0) {
	    	cerr << "empty file" << endl;
	    	exit(0);
	    }
	    if (mode[mode.length() - 1] == '\r') {
			mode.erase(mode.end() - 1);
			win_cr = true;
		} else {
			win_cr = false;
		}
    	file >> language;
    	if (file.fail()) {
    		cerr << "wrong format of language" << endl;
    		exit(0);
    	}
    	file >> a;
    	if (file.fail()) {
    		cerr << "wrong format of shift" << endl;
    		exit(0);
    	}
    	if (language == "english") {
    		alph = alph_eng_high;
    	} else if (language == "russian") {
    		alph = alph_rus;
    	} else {
    		cerr << "wrong language: " << language << endl;
    		exit(1);
    	}
    	if (a < 0) {
    		left_side_rot = false;
    		a = -a;
    	} else {
    		left_side_rot = true;
    	}
    	r = sqrt(alph.length()); 
    	file.ignore(win_cr ? 2 : 1); // miss '\n'
    	getline(file, text);
    	if (text.length() == 0) {
    		cerr << "Error: empty string" << endl;
    		exit(1);
    	}
    	if (win_cr) {
			if (text[text.length() - 1] == '\r') {
				text.erase(text.end() - 1);
			}
		}
    	a = a % (text.length() * 2);
    	if (a % 2 != 1) {
    		cerr << "Error: a % 2 == 0" << endl;
    		exit(1);
    	}
    	if (mode == "encrypt") {
    		if (left_side_rot) {
 		   		encrypt_proc();
    		} else {
    			decrypt_proc();
    		}
    	} else if (mode == "decrypt") {
    		if (left_side_rot) {
    			decrypt_proc();
    		} else {
 		   		encrypt_proc();
    		}
    	} else {
    		cerr << "wrong command: " << mode << endl;
    		exit(1);
    	}
    	file.close();
	}
	~task3() = default;
private:
	unsigned int conv_char_to_int(char c) {
	    int pos = alph.find(c);
		if (pos == (int)std::string::npos) {
		    cerr << "unknown symbol: " << c << endl;
			exit(1);
		}
		return (pos / r + 1) * 10 + (pos % r + 1);
	}
	char conv_int_to_char(unsigned int a) {
	    if (!(a % 10 >= 1 && a % 10 <= (unsigned)r && a / 10 >= 1 && a / 10 <= (unsigned)r && a / 100 == 0)) {
		    cerr << "internal error" << endl;
			exit(1);
		}
		a = (a / 10 - 1) * 10 + a % 10 - 1;
		return alph[a % 10 + (a / 10) * r];
	}
	void encrypt_proc() {
	    vector<int> vec, save;
		vector<bool> low_case;
		if (language == "english") {
	    	for (unsigned int i = 0; i < text.length(); i++) {
	    		if (text[i] == 'J') {
					text[i] = 'I';
				}
				if (text[i] == 'j') {
					text[i] = 'i';
				}
				char c = text[i];
				int pos = alph_eng_low.find(c);
				if (pos == (int)std::string::npos) {
				    pos = alph_eng_high.find(c);
					if (pos == (int)std::string::npos) {
					    cerr << "unknown symbol: " << c << endl;
						exit(1);
					}
					low_case.push_back(false);
				} else {
					text[i] = alph[pos];
					low_case.push_back(true);
				}
			}
		}
		int first;
		for (unsigned int i = 0; i < text.length(); i++) {
			vec.push_back(conv_char_to_int(text[i]));
		}
		for (int i = 0; i < a / 2; i++) {
			save.push_back(vec[i]);
		}	
		for (unsigned int i = 0; i < vec.size() - a / 2; i++) {
			vec[i] = vec[i + a / 2];
		}
		for (int i = 0; i < a / 2; i++) {
			vec[vec.size() - 1 - i] = save[a / 2 - 1 - i];
		}
		first = vec[0] / 10;
		for (unsigned int i = 0; i < vec.size() - 1; i++) {
			vec[i] = vec[i] % 10 * 10 + vec[i + 1] / 10;
		}
		vec[vec.size() - 1] = vec[vec.size() - 1] % 10 * 10 + first;
		for(unsigned int i = 0; i < vec.size(); i++) {
			text[i] = conv_int_to_char(vec[i]);
		}
		if (language == "english") {
	    	for (unsigned int i = 0; i < text.length(); i++) {
				if (low_case[i]) {
					text[i] = alph_eng_low[alph.find(text[i])];
				}
			}
		}
		cout << text << endl;
		return;
	}
	void decrypt_proc() {
		vector<int> vec, save;
    	vector<bool> low_case;
		if (language == "english") {
	    	for (unsigned int i = 0; i < text.length(); i++) {
				if (text[i] == 'J') {
					text[i] = 'I';
				}
				if (text[i] == 'j') {
					text[i] = 'i';
				}
				char c = text[i];
				int pos = alph_eng_low.find(c);
				if (pos == (int)std::string::npos) {
				    pos = alph_eng_high.find(c);
					if (pos == (int)std::string::npos) {
					    cerr << "unknown symbol: " << c << endl;
						exit(1);
					}
					low_case.push_back(false);
				} else {
					text[i] = alph[pos];
					low_case.push_back(true);
				}
			}
		}
		int last;
		for (unsigned int i = 0; i < text.length(); i++) {
			vec.push_back(conv_char_to_int(text[i]));
		}
		last = vec[vec.size() - 1] % 10;
		for (unsigned int i = vec.size() - 1; i > 0; i--) {
			vec[i] = vec[i - 1] % 10 * 10 + vec[i] / 10;
		}
		vec[0] = vec[0] / 10 + last * 10;
		for (unsigned int i = vec.size() - a / 2; i < vec.size(); i++) {
			save.push_back(vec[i]);
		}
		for (int i = vec.size() - 1; i >= a / 2; i--) {
			vec[i] = vec[i - a / 2];
			if (i == 0) {
				break;
			}
		}
		for (int i = 0; i < a / 2; i++) {
			vec[i] = save[i];
		}
		for(unsigned int i = 0; i < vec.size(); i++) {
			text[i] = conv_int_to_char(vec[i]);
		}
		if (language == "english") {
	    	for (unsigned int i = 0; i < text.length(); i++) {
				if (low_case[i]) {
					text[i] = alph_eng_low[alph.find(text[i])];
				}
			}
		}
		cout << text << endl;
		return;
	}
};

int main(int argc, char **argv) {
	task3 prog(argv[1]);
    return 0;
}
