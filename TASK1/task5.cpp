#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

using namespace std;

class task5 {
private:
	string mode, keyword, text;
	const string alph_high = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
	const string alph_low = "abcdefghiklmnopqrstuvwxyz";
	const string delims = ",.:_?!";
	ifstream file;
	char matrix[5][5];
	bool low_case[2], win_cr; 
public:
	task5(char *f_name) {
		ifstream file(f_name);
	    if (!file.is_open()) {
	        cerr << "file cant be open" << endl;
	    	exit(0);
	    }
    	getline(file, mode);
    	if (mode.length() == 0) {
	    	cerr << "empty file" << endl;
	    	exit(0);
	    }
    	if (mode[mode.length() - 1] == '\r') {
			mode.erase(mode.end() - 1);
			win_cr = true;
		} else {
			win_cr = false;
		}
		getline(file, keyword);
    	if (win_cr) {
			if (keyword[keyword.length() - 1] == '\r') {
				keyword.erase(keyword.end() - 1);
			} else {
				cerr << "using \\n and \\r\\n simulteniously" << endl;
				exit(0);
			}
		}
    	getline(file, text);
    	if (win_cr) {
			if (text[text.length() - 1] == '\r') {
				text.erase(text.end() - 1);
			}
		}
    	fill_matrix();
    	if (text.length() == 0) {
    		cout << endl;
    		exit(0);
    	}
    	if (mode == "encrypt") {
    		encrypt_proc();
    	} else if (mode == "decrypt") {
    		decrypt_proc();
    	} else {
    		cerr << "wrong command: " << mode << endl;
    		exit(0);
    	}
    	file.close();
	}
	~task5() = default;
private:
	// fill matrix using high characters
	void fill_matrix() {
		int pos;
		bool repeated;
		char c;
		for (unsigned int i = 0; i < keyword.length(); i++) {
			if (keyword[i] == 'j') {
				keyword[i] = 'i';
			}
			if (keyword[i] == 'J') {
				keyword[i] = 'I';
			}
			c = keyword[i];
			pos = alph_low.find(c);
			if (pos == (int)std::string::npos) {
			    pos = alph_high.find(c);
				if (pos == (int)std::string::npos) {
				    cerr << "unknown symbol in keyword: " << c << endl;
					exit(1);
				}
			} else {
				keyword[i] = alph_high[pos];
			}
		}
		pos = 0;
		for (unsigned int i = 0; i < keyword.length(); i++) {
			repeated = false;
			for (auto j = 0; j < pos; j++) {
				if (keyword[i] == matrix[j / 5][j % 5]) {
					repeated = true;
					break;
				}
			}
			if (!repeated) {
				matrix[pos / 5][pos % 5] = keyword[i];
				pos++;
			}
			if (pos == 25) {
				return;
			}
		}
		for (unsigned int i = 0; i < alph_high.length(); i++) {
			repeated = false;
			for (auto j = 0; j < pos; j++) {
				if (alph_high[i] == matrix[j / 5][j % 5]) {
					repeated = true;
					break;
				}
			}
			if (!repeated) {
				matrix[pos / 5][pos % 5] = alph_high[i];
				pos++;
			}
			if (pos == 25) {
				return;
			}
		}
		return;
	}
	int get_line_num(char c) {
		for (auto i = 0; i < 5; i++) {
			for (auto j = 0; j < 5; j++) {
				if (c == matrix[i][j]) {
					return i;
				}
			}
		}
		return 0;
	}
	int get_column_num(char c) {
		for (auto i = 0; i < 5; i++) {
			for (auto j = 0; j < 5; j++) {
				if (c == matrix[i][j]) {
					return j;
				}
			}
		}
		return 0;
	}
	void to_high(char &c1, char &c2) {
		int pos;
		pos = alph_low.find(c1);
		if (pos == (int)std::string::npos) {
		    pos = alph_high.find(c1);
			if (pos == (int)std::string::npos) {
			    cerr << "unknown symbol: " << c1 << endl;
				exit(0);
			}
			low_case[0] = false;
		} else {
			low_case[0] = true;
			c1 = alph_high[pos];
		}
		pos = alph_low.find(c2);
		if (pos == (int)std::string::npos) {
		    pos = alph_high.find(c2);
			if (pos == (int)std::string::npos) {
			    cerr << " unknown symbol: " << c2 << endl;
				exit(0);
			}
			low_case[1] = false;
		} else {
			low_case[1] = true;
			c2 = alph_high[pos];
		}
		return;
	}
	void encrypt_proc() {
	    unsigned int i1, i2, i = 0;
	    int l_nspace, pos;
	    char save;
	    for (l_nspace = text.length() - 1; l_nspace >= 0; l_nspace--) {
	    	if (text[l_nspace] != ' ') {
	    		break;
	    	}
	    }
	    if (l_nspace == -1) {
	    	cout << text << endl;
	    	exit(0);
	    }
	    while (i < text.length()) {
	    	if (text[i] == ' ' || (pos = delims.find(text[i])) != (int)std::string::npos) {
	    		i++;
	    		continue;
	    	}
	    	i1 = i;
	    	i++;
	    	while (text[i] == ' ' || (pos = delims.find(text[i])) != (int)std::string::npos) {
	    		i++;
	    	}
	    	if (i == text.length()) {
	    		text += 'X';
	    	}
	    	i2 = i;
	    	if (text[i1] == 'J') {
	    		text[i1] = 'I';
	    	}
	    	if (text[i2] == 'J') {
	    		text[i2] = 'I';
	    	}
	    	if (text[i1] == 'j') {
	    		text[i1] = 'i';
	    	}
	    	if (text[i2] == 'j') {
	    		text[i2] = 'i';
	    	}
	    	to_high(text[i1], text[i2]);
		    if (text[i1] == text[i2]) {
		    	if (!(text[i] == 'X' && i == text.length() - 1)) {
			    	text += '0';
			    	for (auto j = text.length() - 1; j > i; j--) {
			    		text[j] = text[j - 1];
			    	}
			    	text[i] = 'X';
			    	if (low_case[1]) {
			    		text[i + 1] = alph_low[alph_high.find(text[i + 1])];
			    	}
			    	low_case[1] = false;
			    }
		    }	    	
	    	if (get_line_num(text[i1]) == get_line_num(text[i2])) {
	    		text[i1] = matrix[get_line_num(text[i1])][(get_column_num(text[i1]) + 1) % 5];
	    		text[i2] = matrix[get_line_num(text[i2])][(get_column_num(text[i2]) + 1) % 5];
	    	} else if (get_column_num(text[i1]) == get_column_num(text[i2])) {
	    		text[i1] = matrix[(get_line_num(text[i1]) + 1) % 5][get_column_num(text[i1])];
	    		text[i2] = matrix[(get_line_num(text[i2]) + 1) % 5][get_column_num(text[i2])];
	    	} else {
	    		save = text[i1];
	    		text[i1] = matrix[get_line_num(text[i1])][get_column_num(text[i2])];
	    		text[i2] = matrix[get_line_num(text[i2])][get_column_num(save)];
	    	}
	    	if (low_case[0]) {
	    		text[i1] = alph_low[alph_high.find(text[i1])];
	    	}
	    	if (low_case[1]) {
	    		text[i2] = alph_low[alph_high.find(text[i2])];
	    	}
	    	i++;
	    }
		cout << text << endl;
		return;
	}
	void decrypt_proc() {
		unsigned int i1, i2, i = 0;
		int l_nspace, pos;
	    char save;
	    for (l_nspace = text.length() - 1; l_nspace >= 0; l_nspace--) {
	    	if (text[l_nspace] != ' ') {
	    		break;
	    	}
	    }
	    if (l_nspace == - 1) {
	    	cout << text << endl;
	    	exit(0);
	    }
	    while (i < text.length()) {
	    	if (text[i] == ' ' || (pos = delims.find(text[i])) != (int)std::string::npos) {
	    		i++;
	    		continue;
	    	}
	    	i1 = i;
	    	i++;
	    	while (text[i] == ' ' || (pos = delims.find(text[i])) != (int)std::string::npos) {
	    		if (i == text.length() - 1) {
	    			cerr << "chiper text must have length 2 * k" << endl;
	    			exit(0);
	    		} else {
	    			;
	    		}
	    		i++;
	    	}
	    	i2 = i;
	    	if (text[i1] == 'J') {
	    		text[i1] = 'I';
	    	}
	    	if (text[i2] == 'J') {
	    		text[i2] = 'I';
	    	}
	    	if (text[i1] == 'j') {
	    		text[i1] = 'i';
	    	}
	    	if (text[i2] == 'j') {
	    		text[i2] = 'i';
	    	}
	    	to_high(text[i1], text[i2]);
	    	if (get_line_num(text[i1]) == get_line_num(text[i2])) {
	    		text[i1] = matrix[get_line_num(text[i1])][(get_column_num(text[i1]) + 4) % 5];
	    		text[i2] = matrix[get_line_num(text[i2])][(get_column_num(text[i2]) + 4) % 5];
	    		if (text[i2] == 'X') {
	    			if (i2 == (unsigned)l_nspace) { 
	    				text.erase(text.end() - 1);
	    			} else {
	    				text[i2] = text[i1];
	    			}
	    		}
	    	} else if (get_column_num(text[i1]) == get_column_num(text[i2])) {
	    		text[i1] = matrix[(get_line_num(text[i1]) + 4) % 5][get_column_num(text[i1])];
	    		text[i2] = matrix[(get_line_num(text[i2]) + 4) % 5][get_column_num(text[i2])];
	    		if (text[i2] == 'X') {
	    			if (i2 == (unsigned)l_nspace) { 
	    				text.erase(text.end() - 1);
	    			} else {
	    				text[i2] = text[i1];
	    			}
	    		}
	    	} else {
	    		save = text[i1];
	    		text[i1] = matrix[get_line_num(text[i1])][get_column_num(text[i2])];
	    		text[i2] = matrix[get_line_num(text[i2])][get_column_num(save)];
	    	}
	    	if (low_case[0]) {
	    		text[i1] = alph_low[alph_high.find(text[i1])];
	    	}
	    	if (low_case[1]) {
	    		text[i2] = alph_low[alph_high.find(text[i2])];
	    	}
	    	i++;
	    }
		cout << text << endl;
		return;
	}
};

int main(int argc, char **argv) {
	task5 prog(argv[1]);
    return 0;
}