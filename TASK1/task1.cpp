#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <vector>

using namespace std;

class task1 {
private:
	int a, b, pos;
	string mode, text;
	const string alph_high = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	const string alph_low = "abcdefghijklmnopqrstuvwxyz";
	const unsigned int m = alph_high.length();
    ifstream file;
    char x1, x2, y1, y2;
    bool low_case, win_cr;
public:
	task1(char *f_name) {
		ifstream file(f_name);
	    if (!file.is_open()) {
	        cerr << "file cant be open!" << endl;
	        exit(0);
	    }
	    getline(file, mode);
		if (mode[mode.length() - 1] == '\r') {
			mode.erase(mode.end() - 1);
			win_cr = true;
		} else {
			win_cr = false;
		}
    	if (mode == "encrypt") {
    		file >> a >> b;
    		file.ignore(win_cr ? 2 : 1); // miss '\n'
    		getline(file, text);
    		if (win_cr) {
    			if (text[text.length() - 1] == '\r') {
					text.erase(text.end() - 1);
    			}
			}
			while (a < 0) {
				a += m;
			}
			while (b < 0) {
				b += m;
			}
			a = a % m;
    		b = b % m;
    		encrypt_proc();
    	} else if (mode == "decrypt") {
    		file >> a >> b;
    		file.ignore(win_cr ? 2 : 1); // miss '\n'
    		getline(file, text);
    		if (win_cr) {
    			if (text[text.length() - 1] == '\r') {
					text.erase(text.end() - 1);
    			}
			}
			while (a < 0) {
				a += m;
			}
			while (b < 0) {
				b += m;
			}
			a = a % m;
    		b = b % m;
    		decrypt_proc();
    	} else if (mode == "break") {
    		file >> x1 >> x2 >> y1 >> y2;
			file.ignore(win_cr ? 2 : 1); // miss '\n'
			getline(file, text);
			if (win_cr) {
    			if (text[text.length() - 1] == '\r') {
					text.erase(text.end() - 1);
    			}
			}
    		break_proc();
    	} else {
    		cerr << "wrong command: " << mode << endl;
    		exit(1);
    	}
    	file.close();
	}
	~task1() = default;
private:
	// ax + by = gcd(a, b)
	unsigned int gcd(int a, int b, int &x, int &y) {
		int x1, y1;
		if (a == 0) {
			x = 0;
			y = 1;
			return b;
		}
		unsigned int d = gcd(b % a, a, x1, y1);
		x = y1 - (b / a) * x1;
		y = x1;
		return d;
	}
	unsigned int conv_char_to_int(char c) {
		pos = alph_low.find(c);
		if (pos == (int)std::string::npos) {
		    pos = alph_high.find(c);
			if (pos == (int)std::string::npos) {
			    cerr << "unknown symbol: " << c << endl;
				exit(1);
			}
			low_case = false;
		} else {
			low_case = true;
		}
		return pos;
	}
	void encrypt_proc() {
	    for (unsigned int i = 0; i < text.length(); i++) {
			if (text[i] != ' ' && text[i] != '_') {
				text[i] = alph_high[(conv_char_to_int(text[i]) * a + b) % m];
			} else {
				continue;
			}
			if (low_case) {
				text[i] = alph_low[alph_high.find(text[i])];
			}
		}
		cout << text << endl;
		return;
	}
	void decrypt_proc() {
		int x, y;
		if (gcd(a, m, x, y) != 1) {
			cerr << "cant decrypt because a and m aren't coprime" << endl;
			exit(1);
		}
		while (x < 0) {
			x += m;
		}
		for (unsigned i = 0; i < text.length(); i++) {
			if (text[i] != ' ' && text[i] != '_') {
				text[i] = alph_high[(conv_char_to_int(text[i]) - b + m) * x % m];
				if (low_case) {
					text[i] = alph_low[alph_high.find(text[i])];
				}
			}
		}
		cout << text << endl;
		return;
	}
	void break_proc() {
		int x, y;
		x1 = conv_char_to_int(x1);
		x2 = conv_char_to_int(x2);
		y1 = conv_char_to_int(y1);
		y2 = conv_char_to_int(y2);
		if (x1 == x2 || y1 == y2) {
			cerr << "cant decrypt because x1 == x2 or y1 == y2" << endl;
			return;
		}
		if (gcd(x1 - x2 + m, m, x, y) != 1) {
			cerr << "cant decrypt because x1 - x2 and m aren't coprime" << endl;
			return;
		}
		while (x < 0) {
			x += m;
		}
		a = (x * (y1 - y2 + m)) % m;
		b = (x * (y2 * x1 - y1 * x2 + m * m)) % m;
		decrypt_proc();
		return;
	}
};

int main(int argc, char **argv) {
	task1 prog(argv[1]);
    return 0;
}
