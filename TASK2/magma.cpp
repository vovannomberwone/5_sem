// n = 64

#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <inttypes.h>
#include <cstring>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

class request {
private:
	friend class scrambler;
	enum mode {
		default_mode,
		ecb,
		ctr,
		ofb,
		cbc,
		cfb,
		mac,
		help
	} m_type;
	enum action {
		default_action,
		encrypt,
		decrypt
	} a_type;
	char *input, *output, *key_file, *iv_file;
	const char *m_types[10] = {
		"default", "--ecb", "--ctr", "--ofb", "--cbc", "--cfb", "--mac", "--help", "-h"
	};
	const char *a_types[10] = {
		"default", "-e", "-d"
	};
	bool m_type_check(char *str) {
		for (auto i = 1; i < 9; i++) {
			if (strcmp(m_types[i], str) == 0) {
				if (m_type != default_mode) {
					cerr << "try using several modes simultaneously" << endl;
					exit(0);
				} else {
					if (i == 8) {
						m_type = (mode)7;
					} else {
						m_type = (mode)i;
					}
					return true;
				}
			}
		}
		return false;
	}
	bool a_type_check(char *str) {
		for (auto i = 1; i < 3; i++) {
			if (strcmp(a_types[i], str) == 0) {
				if (a_type != default_action) {
					cerr << "try using several actions simultaneously" << endl;
					exit(0);
				} else {
					a_type = (action)i;
					return true;
				}
			}
		}
		return false;
	}
public:
	request() = default;
	request(const request &r) {
		m_type = r.m_type;
		a_type = r.a_type;
		input = r.input;
		output = r.output;
		key_file = r.key_file;
		iv_file = r.iv_file;
	}
	request(int argc, char **argv) {
		m_type = default_mode;
		a_type = default_action;
		input = nullptr;
		output = nullptr;
		iv_file = nullptr;
		for (auto i = 1; i < argc; i++) {
			if (strcmp(argv[i], "-k") == 0) {
				i++;
				if (i == argc) {
					cerr << "key file is absent" << endl;
					exit(0);
				}
				key_file = argv[i];
				continue;
			} else if (strcmp(argv[i], "-i") == 0) {
				i++;
				if (i == argc) {
					cerr << "input file is absent" << endl;
					exit(0);
				}
				input = argv[i];
				continue;
			} else if (strcmp(argv[i], "-o") == 0) {
				i++;
				if (i == argc) {
					cerr << "output file is absent" << endl;
					exit(0);
				}
				output = argv[i];
				continue;
			} else if (strcmp(argv[i], "-v") == 0) {
				i++;
				if (i == argc) {
					cerr << "IV file is absent" << endl;
					exit(0);
				}
				iv_file = argv[i];
				continue;
			} else {
				if (m_type_check(argv[i])) {
					continue;
				} else if (a_type_check(argv[i])) {
					continue;
				} else {
					cerr << "unknown option: " << argv[i] << endl;
					exit(0);
				}
			}
		}
		if (m_type == help && (a_type != default_action || key_file != nullptr
		|| input != nullptr || output != nullptr)) {
			cerr << "incorrect syntax for help mode" << endl;
			exit(0);
		}
		if (m_type == mac && (a_type != default_action || key_file == nullptr
		|| iv_file != nullptr)) {
			cerr << "incorrect syntax for mac mode" << endl;
			exit(0);
		}
		if (m_type == default_mode && (m_type != help)) {
			m_type = ecb;
		}
		if (m_type == ecb && (a_type == default_action || key_file == nullptr
		|| iv_file != nullptr)) {
			cerr << "incorrect syntax for ecb mode" << endl;
			exit(0);
		}
		if ((m_type == ctr || m_type == ofb || m_type == cbc || m_type == cfb) 
		&& (a_type == default_action || key_file == nullptr)) {
			cerr << "incorrect syntax for ctr or ofb or cbc or cfb mode" << endl;
			exit(0);
		}
	}
	~request() = default;
};

class scrambler {
private:
	const string info = "\
		magma [-h|--help]\n\
		magma [--ecb|--ctr|--ofb|--cbc|--cfb] {-e|-d} -k <key file> [options]\n\
		magma --mac -k <key file> [options]\n\
		-h | --help - вывести текущее описание в stdout\n\
		Режимы работы, по умолчанию используется режим ECB:\n\
		--ecb - ГОСТ Р 34.13-2015, пункт 5.1\n\
		--ctr - ГОСТ Р 34.13-2015, пункт 5.2\n\
		--ofb - ГОСТ Р 34.13-2015, пункт 5.3\n\
		--cbc - ГОСТ Р 34.13-2015, пункт 5.4\n\
		--cfb - ГОСТ Р 34.13-2015, пункт 5.5\n\
		--mac - ГОСТ Р 34.13-2015, пункт 5.6\n\
		-e - произвести зашифрование.\n\
		-d - произвести расшифрование.\n\
		-k <key file> - файл с бинарным ключом.\n\
		[options]:\n\
		-v <iv file> - файл с бинарным значением IV. По умолчанию IV=0;\n\
		-i <input file> - входной файл. По умолчанию читать с stdin до EOF;\n\
		-o <output file> - выходной файл. По умолчанию выводить в stdout.\n\
	";
	const request &rqst;
	int fd_key, fd_inp, fd_out, n, fd_iv;
	uint64_t block, chiper_block;
	char c; // buf for testing that file is ended
	uint32_t primary_key[8];
	uint32_t keys[32];
	const unsigned char pi[8][16] = {
		{12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1},
		{6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11, 13, 0, 15},
		{11, 3, 5, 8, 2, 15, 10, 13, 14, 1, 7, 4, 12, 9, 6, 0},
		{12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11},
		{7, 15, 5, 10, 8, 1, 6, 13, 0, 9, 3, 14, 11, 4, 2, 12},
		{5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1, 4, 3, 14, 0},
		{8, 14, 2, 5, 6, 9, 1, 12, 15, 4, 11, 0, 13, 10, 3, 7},
		{1, 7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11, 2},
	};
	// Litle Endian -> Big Endian
	uint32_t reverse_int32(const uint32_t a) {
		uint32_t res = 0, temp;
		uint32_t mask = 0xFF000000; 
		for (auto i = 0; i < 4; i++) {
			res = res >> 8;
			temp = a & mask;
			temp = temp << (i * 8);
			res = res | temp;
			mask = mask >> 8;
		}
		return res;
	}
	// Litle Endian -> Big Endian
	uint64_t reverse_int64(const uint64_t a) {
		uint64_t res = 0, temp;
		uint64_t mask = (uint64_t)0xFF << (8 * 7); 
		for (auto i = 0; i < 8; i++) {
			res = res >> 8;
			temp = a & mask;
			temp = temp << (i * 8);
			res = res | temp;
			mask = mask >> 8;
		}
		return res;
	}
	void key_schedule() {
		for (auto i = 0; i < 8; i++) {
			keys[i] = primary_key[i];
		}
		for (auto i = 0; i < 8; i++) {
			keys[i + 8] = keys[i + 16] = keys[i];
			keys[i + 24] = keys[7 - i];
		}
		return;
	}
	uint32_t t_proc(const uint32_t a) {
		unsigned char left, right;
		uint32_t res, mask, temp;
		mask = 0xFF;
		res = 0x00;
		for (auto i = 0; i < 4; i++) {
			left = right = (a & mask) >> (8 * i);
			left = left >> 4;
			right = right & 0x0F;
			left = pi[2 * i + 1][left];
			right = pi[2 * i][right];
			temp = (left << 4) | right;
			temp = temp << (8 * i);
			res = res | temp;
			mask = mask << 8;
		}
		return res;
	}
	uint32_t g_proc(const uint32_t k, const uint32_t a) {
		uint32_t b, c;
		b = c = t_proc(a + k);
		c = c >> (32 - 11);
		b = b << 11;
		return b | c;
	}
	void G_proc(uint32_t &left, uint32_t &right, const uint32_t key) {
		uint32_t save = right;
		right = g_proc(right, key) ^ left;
		left = save;
		return;
	}
	int64_t G_last_proc(const uint32_t left, const uint32_t right, const uint32_t key) {
		int64_t res = (g_proc(right, key) ^ left);
		res = res << 32;
		return res | right;
	}
	int64_t basic_encrypt(const int64_t block) {
		uint32_t left, right;
		left = block >> 32;
		right = block << 32 >> 32;
		for (auto i = 0; i < 31; i++) {
			G_proc(left, right, keys[i]);
		}
		return G_last_proc(left, right, keys[31]);
	}
	int64_t basic_decrypt(const int64_t block) {
		uint32_t left, right;
		left = block >> 32;
		right = block << 32 >> 32;
		for (auto i = 31; i > 0; i--) {
			G_proc(left, right, keys[i]);
		}
		return G_last_proc(left, right, keys[0]);
	}
	void ecb_proc() {
		bool first_iter = true; 
		uint64_t prev_block;

		while ((n = read(fd_inp, &block, 8)) == 8) {
			block = reverse_int64(block);
			switch (rqst.a_type) {
				case request::encrypt: {
					chiper_block = basic_encrypt(block);
				} break;
				case request::decrypt: {
					chiper_block = basic_decrypt(block);
				} break;
				case request::default_action: {
					cerr << "Internal error" << endl;
					exit(0);
				} break;
			}

			chiper_block = reverse_int64(chiper_block);
			if (!(first_iter)) {
				write(fd_out, &prev_block, 8);
			} else {
				first_iter = false;
			}
			prev_block = chiper_block;
		}
		switch (rqst.a_type) {
			case request::encrypt: {
				write(fd_out, &prev_block, 8);
				block = reverse_int64(block);
				for (auto i = 0; i < 7 - n; i++) {
					((unsigned char*)(&block))[i] = 0x00;	
				}
				((unsigned char*)(&block))[7 - n] = 0x80; // 0x80 == b10000000
				chiper_block = basic_encrypt(block);
				chiper_block = reverse_int64(chiper_block);
				write(fd_out, &chiper_block, 8);
			} break;
			case request::decrypt: {
				if (n != 0) {
					cerr << "Text length must be 64 * n" << endl;
					exit(0);
				}
			} break;
			case request::default_action: {
				cerr << "Internal error" << endl;
				exit(0);
			} break;
		}
		return;
	}
	void ctr_proc() {
		//s = 64;

		//open iv file
		uint32_t IV;
		if (fd_iv != 0) {
			if (read(fd_iv, &IV, 4) < 4) {
				cerr << "IV must have 32 bit length" << endl;
				exit(0);
			}
			if (read(fd_iv, &c, 1) != 0) {
				cerr << "IV must have 32 bit length" << endl;
				exit(0);
			}
			IV = reverse_int32(IV);
		} else {
			IV = 0x00;
		}

		uint64_t CTR = ((uint64_t)IV << 32);
		while ((n = read(fd_inp, &block, 8)) == 8) {
			block = reverse_int64(block);
			chiper_block = basic_encrypt(CTR);
			chiper_block = chiper_block >> 0;
			chiper_block = chiper_block ^ block;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, &chiper_block, 8);
			CTR++;
		}

		if (n != 0) {
			block = reverse_int64(block);
			chiper_block = basic_encrypt(CTR);
			chiper_block = chiper_block ^ block;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, (unsigned char *)&chiper_block + 8 - n, n);
		}
		return;
	}
	void ofb_proc() {
		//s = 64; m = 128;

		//open iv file
		uint64_t IV[2], temp;
		if (fd_iv != 0) {
			if (read(fd_iv, &IV, 16) < 16) {
				cerr << "IV must have 128 bit length" << endl;
				exit(0);
			}
			if (read(fd_iv, &c, 1) != 0) {
				cerr << "IV must have 128 bit length" << endl;
				exit(0);
			}
			for (auto i = 0; i < 2; i++) {
				IV[i] = reverse_int64(IV[i]);
			}
		} else {
			for (auto i = 0; i < 2; i++) {
				IV[i] = 0x00;	
			}
		}

		while ((n = read(fd_inp, &block, 8)) == 8) {
			block = reverse_int64(block);
			temp = chiper_block = basic_encrypt(IV[0]);
			chiper_block = chiper_block ^ block;
			IV[0] = IV[1];
			IV[1] = temp;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, &chiper_block, 8);
		}
		if (n != 0) {
			chiper_block = basic_encrypt(IV[0]);
			block = reverse_int64(block);
			chiper_block = chiper_block ^ block;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, (unsigned char *)&chiper_block + 8 - n, n);
		}
		return;
	}

	// does not clean padding
	void cbc_proc() {
		// m = 192;

		//open iv file
		uint64_t IV[3];
		if (fd_iv != 0) {
			if (read(fd_iv, &IV, 24) < 24) {
				cerr << "IV must have 192 bit length" << endl;
				exit(0);
			}
			if (read(fd_iv, &c, 1) != 0) {
				cerr << "IV must have 192 bit length" << endl;
				exit(0);
			}
			for (auto i = 0; i < 3; i++) {
				IV[i] = reverse_int64(IV[i]);
			}
		} else {
			for (auto i = 0; i < 3; i++) {
				IV[i] = 0x00;	
			}
		}

		switch (rqst.a_type) {
			case request::encrypt: {
				while ((n = read(fd_inp, &block, 8)) == 8) {
					block = reverse_int64(block);
					chiper_block = IV[0] ^ block;
					chiper_block = basic_encrypt(chiper_block);
					IV[0] = IV[1];
					IV[1] = IV[2];
					IV[2] = chiper_block;
					chiper_block = reverse_int64(chiper_block);
					write(fd_out, &chiper_block, 8);
				}
				block = reverse_int64(block);
				for (auto i = 0; i < 7 - n; i++) {
					((unsigned char*)(&block))[i] = 0x00;	
				}
				((unsigned char*)(&block))[7 - n] = 0x80; // 0x80 == b10000000
				chiper_block = IV[0] ^ block;
				chiper_block = basic_encrypt(chiper_block);
				chiper_block = reverse_int64(chiper_block);
				write(fd_out, &chiper_block, 8);
			} break;
			case request::decrypt: {
				while ((n = read(fd_inp, &block, 8)) == 8) {
					block = reverse_int64(block);
					chiper_block = basic_decrypt(block);
					chiper_block = chiper_block ^ IV[0];
					IV[0] = IV[1];
					IV[1] = IV[2];
					IV[2] = block;
					chiper_block = reverse_int64(chiper_block);
					write(fd_out, &chiper_block, 8);
				}
				if (n != 0) {
					cerr << "Text length must be 64 * n" << endl;
					exit(0);
				}
			} break;
			case request::default_action: {
				cerr << "Internal error" << endl;
				exit(0);
			} break;
		}
		return;
	}

	// does not clean padding
	void cfb_proc() {
		//s = 64; m = 128;

		//open iv file
		uint64_t IV[2];
		if (fd_iv != 0) {
			if (read(fd_iv, &IV, 16) < 16) {
				cerr << "IV must have 128 bit length" << endl;
				exit(0);
			}
			if (read(fd_iv, &c, 1) != 0) {
				cerr << "IV must have 128 bit length" << endl;
				exit(0);
			}
			for (auto i = 0; i < 2; i++) {
				IV[i] = reverse_int64(IV[i]);
			}
		} else {
			for (auto i = 0; i < 2; i++) {
				IV[i] = 0x00;	
			}
		}

		switch (rqst.a_type) {
			case request::encrypt: {
				while ((n = read(fd_inp, &block, 8)) == 8) {
					block = reverse_int64(block);
					chiper_block = basic_encrypt(IV[0]);
					chiper_block = chiper_block ^ block;
					IV[0] = IV[1];
					IV[1] = chiper_block;
					chiper_block = reverse_int64(chiper_block);
					write(fd_out, &chiper_block, 8);
				}
				block = reverse_int64(block);
				for (auto i = 0; i < 7 - n; i++) {
					((unsigned char*)(&block))[i] = 0x00;	
				}
				((unsigned char*)(&block))[7 - n] = 0x80; // 0x80 == b10000000
				chiper_block = basic_encrypt(IV[0]);
				chiper_block = chiper_block ^ block;
				
				chiper_block = reverse_int64(chiper_block);
				write(fd_out, &chiper_block, 8);
			} break;
			case request::decrypt: {
				while ((n = read(fd_inp, &block, 8)) == 8) {
					block = reverse_int64(block);
					chiper_block = basic_encrypt(IV[0]);
					chiper_block = chiper_block ^ block;
					IV[0] = IV[1];
					IV[1] = block;
					chiper_block = reverse_int64(chiper_block);
					write(fd_out, &chiper_block, 8);
				}
				if (n != 0) {
					cerr << "chiper text must have len 64 * n" << endl;
					exit(0);
				}
			} break;
			case request::default_action: {
				cerr << "Internal error" << endl;
				exit(0);
			} break;
		}
		return;
	}
	void mac_proc() {
		//s == 32;
		uint64_t R = basic_encrypt(0x00);
		uint64_t K1, K2, prev_block = 0, prev_chiper_block = 0;
		if ((R >> 63) == 0) {
			K1 = R << 1;
		} else {
			K1 = (R << 1) ^ (0x1B); // b0..011011
		}
		if ((K1 >> 63) == 0) {
			K2 = K1 << 1;
		} else {
			K2 = (K1 << 1) ^ (0x1B); // b0..011011
		}
		chiper_block = 0x00;
		while ((n = read(fd_inp, &block, 8)) == 8) {
			prev_chiper_block = chiper_block;
			block = reverse_int64(block);
			chiper_block = chiper_block ^ block;
			chiper_block = basic_encrypt(chiper_block);
			prev_block = block;
		}
		if (n == 0) {
			chiper_block = prev_chiper_block ^ prev_block ^ K1;
			chiper_block = basic_encrypt(chiper_block);
			chiper_block = reverse_int64(chiper_block);
			chiper_block = chiper_block & 0xFFFFFFFF;
			uint32_t res = chiper_block;
			write(fd_out, &res, 4);
			return;
		}
		block = reverse_int64(block);
		for (auto i = 0; i < 7 - n; i++) {
			((unsigned char*)(&block))[i] = 0x00;
		}
		((unsigned char*)(&block))[7 - n] = 0x80; // 0x80 == b10000000
		chiper_block = chiper_block ^ block ^ K2;
		chiper_block = basic_encrypt(chiper_block);
		chiper_block = reverse_int64(chiper_block);
		chiper_block = chiper_block & 0xFFFFFFFF;
		uint32_t res = chiper_block;
		write(fd_out, &res, 4);
		return;
	}
public:
	scrambler(const request &r): rqst(r) {
		//open key, output, input files
		if (rqst.input == nullptr) {
			fd_inp = 0;
		} else {
			fd_inp = open(rqst.input, O_RDONLY);
			if (fd_inp < 0) {
				cerr << "cant open input file" << endl;
				exit(0);
			}
		}
		if (rqst.output == nullptr) {
			fd_out = 1;
		} else {
			fd_out = open(rqst.output, O_WRONLY | O_CREAT | O_TRUNC, 0666);
			if (fd_out < 0) {
				cerr << "cant open output file" << endl;
				exit(0);
			}
		}
		if (rqst.iv_file != nullptr) {
			fd_iv = open(rqst.iv_file, O_RDONLY);
			if (fd_iv < 0) {
				cerr << "cant open iv file" << endl;
				exit(0);
			}
		} else {
			fd_iv = 0;
		}
		if (rqst.key_file != nullptr) {
			fd_key = open(rqst.key_file, O_RDONLY);
			if (fd_key < 0) {
				cerr << "cant open key file" << endl;
				exit(0);
			}
			if (read(fd_key, &primary_key, 32) < 32) {
				cerr << "key must have 256 bit length" << endl;
				exit(0);
			}
			if (read(fd_key, &c, 1) != 0) {
				cerr << "key must have 256 bit length" << endl;
				exit(0);
			}
			for (auto i = 0; i < 8; i++) {
				primary_key[i] = reverse_int32(primary_key[i]);
			}
			key_schedule();
		} else {
			fd_key = 0;
		}

		switch (rqst.m_type) {
			case request::ecb: {
				ecb_proc();
			} break;
			case request::ctr: {
				ctr_proc();
			} break;
			case request::ofb: {
				ofb_proc();
			} break;
			case request::cbc: {
				cbc_proc();
			} break;
			case request::cfb: {
				cfb_proc();
			} break;
			case request::mac: { 
				mac_proc();
			} break;
			case request::help: {
				cout << info << endl;
			} break;
			case request::default_mode: {
				// code never executed here
				cerr << "Internal error" << endl;
				exit(0);
			} break;
		}

		// close all files
		if (fd_inp != 0) {
			close(fd_inp);
		}
		if (fd_out != 1) {
			close(fd_out);
		}
		if (fd_iv != 0) {
			close(fd_iv);
		}
		if (fd_key != 0) {
			close(fd_key);
		}
	}
	~scrambler() = default;
	
};

int main(int argc, char **argv) {
	request rqst(argc, argv);
	scrambler s(rqst);
	return 0;
}