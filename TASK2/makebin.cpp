#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cstdlib>
#include <stdint.h>
#include <cstring>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

int pos(char *str, char c) {
    int pos = 0;
    while (*str != '\0') {
        if (*str == c) {
            return pos;
        }
        pos++;
        str++;
    }
    return -1;
}

int main() {
    int a, b;

    char alph[] = "0123456789abcdef"; 
    char k_name[30], o_name[30];
    printf("Enter input and output files\n");
    scanf("%s%s", k_name, o_name);
    FILE* key = fopen(k_name, "rb");
    int fd_out = open(o_name, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    while ((a = fgetc(key)) != EOF) {
        b = fgetc(key);
        if (b == EOF) {
            printf("ERROR: NOT COMPLETE BYTE\n");
            return 0;
        }
        unsigned char c = pos(alph, a);
        c = c << 4;
        c = c | pos(alph, b);
        write(fd_out, &c, 1);
    }
    close(fd_out);
    return 0;
}