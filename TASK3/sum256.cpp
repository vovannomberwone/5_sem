// n = 256

#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <inttypes.h>
#include <cstring>
#include <string>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <vector>

#include "hash.h"

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::hex;

class sum256_parser {
private:

	class request {
	public:
		bool is_check_option = false;
		enum option {
			default_option,
			binary,
			text,
			help,
		} option_type;
		enum check_options {
			ignore_missing_enable,
			quiet_enable,
			status_enable,
			strict_enable,
			warning_enable
		} check_options_enum;
		bool check_options[5];
		vector <char*> files;
		const char *all_options_long[9] = {
			"default", "--binary", "--text", "--help",
			"--ignore-missing", "--quiet", "--status", "--strict", "--warn"
		};
		const char *all_options_short[9] = {
			"default", "-b", "-t", "--help", "--ignore-missing",
			"--quiet", "--status", "--strict", "-w"
		};
		bool option_type_check(char *str) {
			for (auto i = 1; i < 4; i++) {
				if (strcmp(all_options_long[i], str) == 0 || strcmp(all_options_short[i], str) == 0) {
					if (option_type != option::default_option) {
						cerr << "try using several options simultaneously" << endl;
						exit(1);
					}
					option_type = (option)i;
					return true;
				}
			}
			for (unsigned i = 4; i < sizeof(all_options_long) / sizeof(char*); i++) {
				if (strcmp(all_options_long[i], str) == 0 || strcmp(all_options_short[i], str) == 0) {
					check_options[i - 4] = true;
					return true;
				}
			}
			if (strcmp(str, "-c") == 0 || strcmp(str, "--check") == 0) {
				is_check_option = true;
				return true;
			}
			return false;
		}
		request() {
			option_type = default_option;
		}
		request(const request &r) {
			is_check_option = r.is_check_option;
			option_type = r.option_type;
			for (auto i = 0; i < 5; i++) {
				check_options[i] = r.check_options[i];
			}
			files = r.files;
		}
		request(int argc, char **argv) {
			int j = 1;
			bool next_are_files = false;
			option_type = default_option;
			for (auto i = 0; i < 5; i++) {
				(check_options[i] = false);
			}

			// parse options
			for (int i = 1; i < argc; i++, j++) {
				if (strcmp(argv[i], "-") == 0) {
					break;
				} else if (strcmp(argv[i], "--") == 0) {
					next_are_files = true;
					break;
				} else if (option_type_check(argv[i])) {
					continue;
				} else {
					break;
				}
			}

			// parse file names
			if (j == argc) {
				files.push_back(nullptr);
			} else {
				if (next_are_files) {
					j++;
					if (j == argc) {
						if (files.size() == 0) {
							files.push_back(nullptr);
						} else {	
							files[0] = nullptr;
						}
					}
					for (auto i = j; i < argc; i++) {
						files.push_back(argv[i]);		
					}
				} else {
					for (auto i = j; i < argc; i++) {
						if (strcmp(argv[i], "-") == 0) {
							files.push_back(nullptr);
						} else {
							files.push_back(argv[i]);
						}
					}
				}
			}

			// same options cant be used simulteniously
			if (option_type == help && argc != 2) {
				cerr << "incorrect syntax for help mode" << endl;
				exit(1);
			}
			// use text format default
			if (option_type == option::default_option) {
				option_type = text;
			}
			if (!is_check_option) {
				for (auto i = 0; i < 5; i++) {
					if (check_options[i] != false) {
						cerr << "try using check options for non-check request" << endl;
						exit(1);
					}
				}
			}
		}
		~request() = default;
	};

	const string info = "\
		g256sum [option]... [file]...\n\
		g256sum --help\n\
		\n\
		[file] - название файла (путь к файлу), для которого требуется посчитать\n\
		контрольную сумму. Если параметр отсутствует или равен -, читать со стандартного\n\
		потока ввода.\n\
		\n\
		[option] - один из следующих флагов:\n\
		-b, --binary        чтение в бинарном режиме.\n\
		-t, --text          чтение в текстовом режиме (по умолчанию).\n\
		-c, --check         режим проверки: посчитать значения контрольных сумм и\n\
		                    проверить соответствующие файлы.\n\
		--                  интерпретировать все последующие операнды как названия файлов.\n\
		\n\
		Следующие опции имеют эффект только в режиме проверки:\n\
		--ignore-missing    игнорировать отсутствующие файлы и не выводить сообщение об\n\
		                    их статусе.\n\
		--quet              не печатать OK для удачно прошедших проверку файлов.\n\
		--status            не печатать результаты проверок\n\
		--strict            выход с ненулевым кодом завершения процесса в случае\n\
		                    неправильного формата строки.\n\
		-w, --warn          выводить сообщения о неправильно форматированных строках\n\
		                    в stderr.\n\
		\n\
		--help - вывести описание флагов в stdout.\
	";
	const request rqst;
	// read while ! cur_symb == '\n' or EOF
	void miss(FILE *file) {
		char c;
		int n;

		while (1) {
			n = fread(&c, 1, 1, file);
			if (n == 0 || c == '\n') {
				break;
			}
		}
		return;
	}
	int string_to_int(char str[64], uint64_t val[4]) {
		char symbols[17] = "0123456789abcdef";
		char* pos;
		uint64_t num;

		for (unsigned i = 0; i < 4; i++) {
			num = 0;
			for (unsigned j = 0; j < 16; j++) {
				pos = strchr(symbols, str[i * 16 + j]);
				if (pos != nullptr) {
					num <<= 4;
					num = num | (pos - symbols);
				} else {
					return 1;
				}
			}
			val[i] = num;
		}
		return 0;
	}
	int ret_code = 0;
public:
	~sum256_parser() = default;
	int get_return_code() {
		return ret_code;
	}
	sum256_parser(int argc, char **argv): rqst(argc, argv) {
		uint64_t hash_value[4], hash_to_check[4];
		char hash_to_check_string[64];

		if (rqst.option_type == request::help) {
			cout << info << endl;
			exit(0);
		}
		FILE *file;
		for (unsigned i = 0; i < rqst.files.size(); i++) {
			//open file
			if (rqst.option_type == request::text) {
				if (rqst.files[i] == nullptr) {
					file = freopen(nullptr, "rt", stdin);
				} else {
					file = fopen(rqst.files[i], "rt");
				}
			} else {
				if (rqst.files[i] == nullptr) {
					file = freopen(nullptr, "rb", stdin);
				} else {
					file = fopen(rqst.files[i], "rb");
				}	
			}

			if (file == nullptr) {
				cerr << "g256sum: " << rqst.files[i] << ": No such file or directory" << endl;
				continue;
			}
			if (!rqst.is_check_option) {
				hash h;
				h.hash256(file, hash_value);
				for (auto j = 0; j < 4; j++) {
					cout << hex << hash_value[j];
				}
				if (rqst.option_type == request::text) {
					cout << "  ";
				} else {
					cout << " *";
				}
				cout << rqst.files[i] << endl;
			} else {
				int cant_read = 0, wrong_format = 0, didnt_match = 0, OK_lines = 0;
				int correct_lines = 0;
				int line_num = 1;
				bool end_of_file = false, no_file_was_verified = true;

				while (1) {
					char c;
					int n;
					bool cur_wrong_format = false, is_binary_mode;
					bool cur_cant_read = false, cur_didnt_match = false;
					FILE *cur_file;
					char cur_name[256];

					for (unsigned i = 0; i < 64; i++) {
						n = fread(hash_to_check_string + i, 1, 1, file);
						if (n == 0 && i == 0) {
							end_of_file = true;
							break;
						}
						if (n == 0 || hash_to_check_string[i] == '\n') {
							// wrong format string
							cur_wrong_format = true;
							wrong_format++;
							break;
						}
					}
					if (end_of_file) {
						break;
					}
					if (!cur_wrong_format) {
						// next symbol is ' '
						n = fread(&c, 1, 1, file);
						if (n == 0 || c != ' ') {
							// wrong format string
							cur_wrong_format = true;
							wrong_format++;
							if (c != '\n') {
								miss(file);
							}		
						}
					}
					if (!cur_wrong_format) {
						// next symbol is ' ' or '*'
						n = fread(&c, 1, 1, file);
						if (n == 0 || (c != ' ' && c != '*')) {
							// wrong format string
							cur_wrong_format = true;
							wrong_format++;
							if (c != '\n') {
								miss(file);
							}
						}
						if (c == '*') {
							is_binary_mode = true;
						} else {
							is_binary_mode = false;
						}
					}
					int cur_len = 0;
					if (!cur_wrong_format) {
						// next is string with len < 256
						while (cur_len < 256 - 1) {
							n = fread(&c, 1, 1, file);
							if (n == 0) {
								if (cur_len == 0) {
									// wrong format string
									cur_wrong_format = true;
									wrong_format++;
									miss(file);
									break;
								} else {
									cur_name[cur_len] = '\0';
									break;
								}
							}
							if (c == '\r') {
								n = fread(&c, 1, 1, file);
								if (n == 0 || c!= '\n') {
									// wrong format string
									cur_wrong_format = true;
									wrong_format++;
									miss(file);
									break;
								}
								cur_name[cur_len] = '\0';
								break;
							} else if (c == '\n') {
								cur_name[cur_len] = '\0';
								break;
							} else {
								cur_name[cur_len] = c;
								cur_len++;
							}
						}
					}
					if (!cur_wrong_format) {
						// check if string have len < 255
						if ((cur_len == (256 - 1)) && cur_name[cur_len] != '\0') {
							// wrong format string
							cur_wrong_format = true;
							wrong_format++;
							miss(file);
						}
					}
					if (!cur_wrong_format) {
						correct_lines++;
						// here we have correct string
						if (is_binary_mode) {
							cur_file = fopen(cur_name, "rb");
						} else {
							cur_file = fopen(cur_name, "rt");
						}
						if (cur_file == nullptr) {
							//cerr << "g256sum: " << cur_name << ": No such file or directory" << endl;
							cur_cant_read = true;
							cant_read++;
						} else {
							cur_wrong_format = string_to_int(hash_to_check_string, hash_to_check);
							if (cur_wrong_format) {
								wrong_format++;
								miss(file);
							} else {
								no_file_was_verified = false;
								hash h;
								h.hash256(cur_file, hash_value);
								for (auto i = 0; i < 4; i++) {
									if (hash_value[i] != hash_to_check[i]) {
										cout << hex << hash_value[i] << " " << hash_to_check[i] << endl;
										cur_didnt_match = true;
										didnt_match++;
										break;
									}
								}
								if (!cur_didnt_match) {
									OK_lines++;
								}
							}
						}
					}
					// here we have parsed string
					if (cur_wrong_format) {
						if (rqst.check_options[request::strict_enable]) {
							exit(1);
						}
						if (rqst.check_options[request::warning_enable]) {
							cerr << "g256sum: WARNING: " << rqst.files[i] << ": line" << line_num;
							cerr << ": improperly formatted g256 checksum line" << endl;
						}
					} else {
						if (cur_cant_read) {
							if (!(rqst.check_options[request::ignore_missing_enable])) {
								cout << "g256sum: " << rqst.files[i] << ": " << cur_name << ": FAILED open or read" << endl;
							}
						} else {
							if (!(rqst.check_options[request::status_enable])) {
								if (cur_didnt_match) {
									cout << "g256sum: " << rqst.files[i] << ": " << cur_name << ": FAILED" << endl;
								} else {
									if (!(rqst.check_options[request::quiet_enable])) {
										cout << "g256sum: " << rqst.files[i] << ": " << cur_name << ": OK" << endl;
									}
								}
							}
						}
					}
					line_num++;
				}

				// print warning
				if (!(rqst.check_options[request::status_enable])) { 
					if (cant_read + wrong_format + didnt_match > 0) {	
						if (wrong_format && correct_lines) {
							cerr << "g256sum: WARNING: " << rqst.files[i] << ": ";
							cerr << wrong_format << " lines are improperly formatted" << endl;
						}
						if (cant_read && !(rqst.check_options[request::ignore_missing_enable])) {
							cerr << "g256sum: WARNING: " << rqst.files[i] << ": ";
							cerr << cant_read << " listed file could not be read" << endl;
						}
						if (didnt_match) {
							cerr << "g256sum: WARNING: " << rqst.files[i] << ": ";
							cerr << didnt_match << " computed checksum did NOT match" << endl;
						}
					}
					if (correct_lines == 0) {
						cerr << "g256sum: " << rqst.files[i];
						cerr << ": no properly formatted g256 checksum lines found" << endl;
					}
					if (rqst.check_options[request::ignore_missing_enable] && no_file_was_verified && correct_lines) {
						cerr << "g256sum: WARNING: " << rqst.files[i];
						cerr << ": no file was verified" << endl;
					}
				}
				if ((cant_read && !(rqst.check_options[request::ignore_missing_enable])) || 
				   (correct_lines > 0 && OK_lines == 0) || correct_lines == 0) {
					ret_code = 1;
				}
			}
			if (file != nullptr) {
				fclose(file);
			}
		}
	}
};

int main(int argc, char **argv) {
	sum256_parser parser(argc, argv);
	return parser.get_return_code();
}