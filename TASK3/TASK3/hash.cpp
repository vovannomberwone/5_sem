#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <inttypes.h>
#include <cstring>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stack>

#include "hash.h"

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::stack;

unsigned char hash::pi_proc(const unsigned char c) {
	return pi_arr[c];    	
}

uint64_t hash::l_proc(const uint64_t a) {
	uint64_t mask = (uint64_t)0x01 << 63; 
	uint64_t cur_bit, res = 0;

	for (auto i = 0; i < 64; i++) {
		cur_bit = (mask & a) >> (63 - i);
		//res ^= cur_bit * A_matrix[i];
		if (cur_bit) {
			res ^= A_matrix[i];
		}
		mask >>= 1;
	}
	return res;
}

void hash::X_proc(const uint64_t key[8], const uint64_t a[8], uint64_t res[8]) {
	for (auto i = 0; i < 8; i++) {
		res[i] = a[i] ^ key[i];
	}
	return;
}

void hash::S_proc(const uint64_t a[8], uint64_t res[8]) {
	for (auto i = 0; i < 64; i++) {
		((unsigned char *)(res))[i] = pi_proc(((unsigned char *)(a))[i]);
	}
	return;
}

void hash::P_proc(const uint64_t a[8], uint64_t res[8]) {
	// if a == res
	uint64_t temp[8];

	for (auto i = 0; i < 8; i++) {
		temp[i] = a[i];
	}
	
	for (auto i = 0 ; i < 64; i++) {
		/*
		// LITTLE ENDIAN
		unsigned int t_i = 63 - t_arr[63 - i];
		((unsigned char *)(res))[i / 8 * 8 + 7 - i % 8] = ((unsigned char *)(temp))[t_i / 8 * 8 + 7 - t_i % 8];
		*/
		((unsigned char *)(res))[i / 8 * 8 + 7 - i % 8] = ((unsigned char *)(temp))[i % 8 * 8 + 7 - i / 8];
	}
	return;
}

void hash::L_proc(const uint64_t a[8], uint64_t res[8]) {
	for (auto i = 0; i < 8; i++) {
		res[i] = l_proc(a[i]);
	}
	return;
}

void hash::E_proc(const uint64_t key[8], const uint64_t m[8], uint64_t res[8]) {
	uint64_t temp[8], cur_key[8];

	for (auto i = 0; i < 8; i++) {
		cur_key[i] = key[i];
		temp[i] = m[i];
	}

	for (auto i = 0; i < 12; i++) {
		X_proc(cur_key, temp, temp);
		S_proc(temp, temp);
		P_proc(temp, temp);
		L_proc(temp, temp);
		for (auto j = 0; j < 8; j++) {
			cur_key[j] ^= C[i][j];
		}
		S_proc(cur_key, cur_key);
		P_proc(cur_key, cur_key);
		L_proc(cur_key, cur_key);
	}
	X_proc(cur_key, temp, res);
	return;
}

void hash::gN_proc(const uint64_t h[8], const uint64_t m[8], const uint64_t N[8], uint64_t res[8]) {
	uint64_t temp[8];

	for (auto i = 0; i < 8; i++) {
		temp[i] = h[i] ^ N[i];
	}
	S_proc(temp, temp);
	P_proc(temp, temp);
	L_proc(temp, temp);
	E_proc(temp, m, temp);
	for (auto i = 0; i < 8; i++) {
		res[i] = temp[i] ^ h[i] ^ m[i];
	}
	return;
}

// res = a + b mod 2 ^ 512 
void hash::sum_512(const uint64_t a[8], const uint64_t b[8], uint64_t res[8]) {
	int overflow = 0;

	for (int j = 7; j >= 0; j--) {
		res[j] = a[j] + b[j] + overflow;
		overflow = res[j] < a[j] || res[j] < b[j];
		/*
		if ((a[j] == b[j]) && (a[j] == (uint64_t)-1) && overflow) {
			overflow = 1;
		} else {
			overflow = (res[j] < a[j]) || ((res[j] == a[j]) && (b[j] == (uint64_t)-1) && overflow);
		}
		*/
	}
	return;
}

// Litle Endian -> Big Endian
uint64_t hash::reverse_int64(const uint64_t a) {
	uint64_t res = 0, temp;
	uint64_t mask = (uint64_t)0xFF << (8 * 7); 
	
	for (auto i = 0; i < 8; i++) {
		res = res >> 8;
		temp = a & mask;
		temp = temp << (i * 8);
		res = res | temp;
		mask = mask >> 8;
	}
	return res;
}

void hash::hash256(FILE *file, uint64_t hash_value[4]) {
	uint64_t block[8], first_block[8], m[8];
	uint64_t h[8], N[8], sigma[8];
	stack<uint64_t> text;
	int n, first_block_size;

	// 1 stage
	for (auto i = 0; i < 64; i++) {
		((unsigned char*)h)[i] = 0x01;
		((unsigned char*)sigma)[i] = ((unsigned char*)N)[i] = 0x00;
	}

	// 2 stage
	while (1) {
		n = fread(block, 1, 512 / 8, file);
		if (n < 512 / 8) {
			break;
		}	
	}
	first_block_size = n;
	fseek(file, 0, SEEK_SET);

	// fill first block
	for (auto i = 0; i < 8; i++) {
		first_block[i] = 0x00;
	}
	fread(&(first_block[7 - first_block_size / 8]), first_block_size % 8, 1, file);
	first_block[7 - first_block_size / 8] = reverse_int64(first_block[7 - first_block_size / 8]);
	first_block[7 - first_block_size / 8] >>= (64 - first_block_size % 8 * 8);
	for (auto i = 0; i < first_block_size / 8; i++) {
		fread(&(first_block[8 - first_block_size / 8 + i]), 8, 1, file);
		first_block[8 - first_block_size / 8 + i] = reverse_int64(first_block[8 - first_block_size / 8 + i]);
	}
	((unsigned char*)(&(first_block[7 - first_block_size / 8])))[first_block_size % 8] = 0x01;
	
	unsigned len_in_blocks = 0;
	const unsigned max_len_in_blocks = 1 << 24; // 1024 MBytes
	while (1) {
		if (len_in_blocks > max_len_in_blocks) {
			cerr << "File have size more then 1024 MBytes" << endl;
			exit(1);
		}
		int n = fread(block, 512 / 8, 1, file);
		if (n == 0) {
			break;
		}
		for (auto i = 0; i < 8; i++) {
			block[7 - i] = reverse_int64(block[7 - i]);
			text.push(block[7 - i]);
			//cout << hex << block[i] << endl;
		}
		len_in_blocks++;
	}

	for (unsigned i = 0; i < text.size(); i += 8) {
		for (auto j = 0; j < 8; j++) {
			m[j] = text.top();
			text.pop();
		}
		gN_proc(h, m, N, h);

		// N = N + 256 mod 2^256
		uint64_t const_512[8];
		for (auto j = 0; j < 7; j++) {
			const_512[j] = 0;
		}
		const_512[7] = 512;
		sum_512(N, const_512, N);

		// sigma = sigma + m mod 2^256
		sum_512(sigma, m, sigma);
	}

	// 3 stage
	for (auto i = 0; i < 8; i++) {
		m[i] = first_block[i];
	}
	
	/*
	cout << "FILLED" << endl;
	for (auto i = 0; i < 8; i++) {
		cout << hex << m[i] << endl;
	}
	cout << "FILLED" << endl;
	*/

	gN_proc(h, m, N, h);
	
	uint64_t len_M[8];
	for (auto j = 0; j < 7; j++) {
		len_M[j] = 0;
	}
	len_M[7] = first_block_size * 8;
	sum_512(N, len_M, N);
	sum_512(sigma, m, sigma);

	uint64_t const_0[8];
	for (auto j = 0; j < 8; j++) {
		const_0[j] = 0;
	}
	gN_proc(h, N, const_0, h);
	gN_proc(h, sigma, const_0, h);
	
	for (auto j = 0; j < 4; j++) {
		hash_value[j] = h[j];
	}
	return;
}

void hash::hash512(FILE *file, uint64_t hash_value[8]) {
	uint64_t block[8], first_block[8], m[8];
	uint64_t h[8], N[8], sigma[8];
	stack<uint64_t> text;
	int n, first_block_size;

	// 1 stage
	for (auto i = 0; i < 64; i++) {
		((unsigned char*)h)[i] = 0x00;
		((unsigned char*)sigma)[i] = ((unsigned char*)N)[i] = 0x00;
	}

	// 2 stage
	while (1) {
		n = fread(block, 1, 512 / 8, file);
		if (n < 512 / 8) {
			break;
		}
	}
	first_block_size = n;
	fseek(file, 0, SEEK_SET);

	// fill first block
	for (auto i = 0; i < 8; i++) {
		first_block[i] = 0x00;
	}
	fread(&(first_block[7 - first_block_size / 8]), first_block_size % 8, 1, file);
	first_block[7 - first_block_size / 8] = reverse_int64(first_block[7 - first_block_size / 8]);
	first_block[7 - first_block_size / 8] >>= (64 - first_block_size % 8 * 8);
	for (auto i = 0; i < first_block_size / 8; i++) {
		fread(&(first_block[8 - first_block_size / 8 + i]), 8, 1, file);
		first_block[8 - first_block_size / 8 + i] = reverse_int64(first_block[8 - first_block_size / 8 + i]);
	}
	((unsigned char*)(&(first_block[7 - first_block_size / 8])))[first_block_size % 8] = 0x01;
	
	unsigned len_in_blocks = 0;
	const unsigned max_len_in_blocks = 1 << 24; // 1024 MBytes
	while (1) {
		if (len_in_blocks > max_len_in_blocks) {
			cerr << "File have size more then 1024 MBytes" << endl;
			exit(1);
		}
		int n = fread(block, 512 / 8, 1, file);
		if (n == 0) {
			break;
		}
		for (auto i = 0; i < 8; i++) {
			block[7 - i] = reverse_int64(block[7 - i]);
			text.push(block[7 - i]);
			//cout << hex << block[i] << endl;
		}
		len_in_blocks++;
	}

	for (unsigned i = 0; i < text.size(); i += 8) {
		for (auto j = 0; j < 8; j++) {
			m[j] = text.top();
			text.pop();
		}
		gN_proc(h, m, N, h);

		// N = N + 256 mod 2^256
		uint64_t const_512[8];
		for (auto j = 0; j < 7; j++) {
			const_512[j] = 0;
		}
		const_512[7] = 512;
		sum_512(N, const_512, N);

		// sigma = sigma + m mod 2^256
		sum_512(sigma, m, sigma);
	}

	// 3 stage
	for (auto i = 0; i < 8; i++) {
		m[i] = first_block[i];
	}

	/*
	cout << "FILLED" << endl;
	for (auto i = 0; i < 8; i++) {
		cout << hex << m[i] << endl;
	}
	cout << "FILLED" << endl;
	*/

	gN_proc(h, m, N, h);
	
	
	uint64_t len_M[8];
	for (auto j = 0; j < 7; j++) {
		len_M[j] = 0;
	}
	len_M[7] = first_block_size * 8;
	sum_512(N, len_M, N);
	sum_512(sigma, m, sigma);

	uint64_t const_0[8];
	for (auto j = 0; j < 8; j++) {
		const_0[j] = 0;
	}
	gN_proc(h, N, const_0, h);
	gN_proc(h, sigma, const_0, h);
	
	for (auto j = 0; j < 8; j++) {
		hash_value[j] = h[j];
	}
	return;
}